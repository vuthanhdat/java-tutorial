package lib;

public class Pagination {
	public int limitPerPage=5;
	public int totalRecords=0;
	public int totalPage=0;
	public int pageIndex=1;
	public int firstPos=0;
	
	public Pagination() {
		
	}
	
	public Pagination(int limitPerPage, int totalRecords, int pageIndex) {
		super();
		this.limitPerPage = limitPerPage;
		this.totalRecords = totalRecords;
		this.totalPage = totalRecords/limitPerPage + (totalRecords%limitPerPage!=0?1:0);
		this.pageIndex = pageIndex;
		this.firstPos = (pageIndex-1)*limitPerPage;
	}

	public int getLimitPerPage() {
		return limitPerPage;
	}

	public void setLimitPerPage(int limitPerPage) {
		this.limitPerPage = limitPerPage;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getFirstPos() {
		return (pageIndex-1)*limitPerPage;
	}

	public void setFirstPos(int firstPos) {
		this.firstPos = firstPos;
	}
	
	
}

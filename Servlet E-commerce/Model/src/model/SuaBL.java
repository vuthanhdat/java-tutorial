package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javabeans.Sua;
import lib.SQLConnectLib;

public class SuaBL {
	private SQLConnectLib dbc;

	public SuaBL() {
		dbc = DbContext.getInstance();
	}

	public List<Sua> getAll() {
		List<Sua> Suas = new ArrayList<Sua>();
		PreparedStatement preparedStatement = dbc.returnPrepareStatement("SELECT * FROM sua;");
		ResultSet result;
		try {
			result = preparedStatement.executeQuery();
			Suas =  parseFromResultSet(result);
			return Suas;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Suas;
	}

	public List<Sua> findItem(String name) {
		List<Sua> Suas = new ArrayList<Sua>();
		PreparedStatement preparedStatement = dbc.returnPrepareStatement("SELECT * FROM sua WHERE Ten_sua LIKE ?;");
		ResultSet result;
		try {
			preparedStatement.setString(1, "%" + name + "%");
			result = preparedStatement.executeQuery();
			Suas =  parseFromResultSet(result);
			return Suas;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Suas;
	}

	public Sua getById(String id) {
		Sua Sua = new Sua();
		PreparedStatement preparedStatement = dbc.returnPrepareStatement("SELECT * FROM sua WHERE Ma_sua = ?;");
		ResultSet result;
		try {
			preparedStatement.setString(1, id);
			result = preparedStatement.executeQuery();
			Sua = this.parseFromResultSet1Row(result);
			return Sua;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public Sua getById_02(String id) {
		Sua Sua = new Sua();
		ResultSet result;
		try {
			result = dbc.execSELECTandWHEREQueryWithSingleStatement("sua", "Ma_sua", id);
			Sua = this.parseFromResultSet1Row(result);
			return Sua;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public int getRowCount() {
		return dbc.execCOUNTQuery("sua");
	}

	private List<Sua> parseFromResultSet(ResultSet result) {
		List<Sua> listOfSua = new ArrayList<Sua>();
		try {
			while (result.next()) {
				Sua entity = new Sua();
				entity.setMaSua(result.getString("Ma_sua"));
				entity.setTenSua(result.getString("Ten_sua"));
				entity.setMaHangSua(result.getString("Ma_hang_sua"));
				entity.setMaLoaiSua(result.getString("Ma_loai_sua"));
				entity.setTrongLuong(result.getInt("Trong_luong"));
				entity.setDonGia(result.getInt("Don_gia"));
				entity.setTpDinhDuong(result.getString("TP_Dinh_Duong"));
				entity.setLoiIch(result.getString("Loi_ich"));
				entity.setHinh(result.getString("Hinh"));
				listOfSua.add(entity);
			}
			return listOfSua;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Sua parseFromResultSet1Row(ResultSet result) {
		Sua entity = new Sua();
		try {
			while (result.next()) {
				//Sua entity = new Sua();
				entity.setMaSua(result.getString("Ma_sua"));
				entity.setTenSua(result.getString("Ten_sua"));
				entity.setMaHangSua(result.getString("Ma_hang_sua"));
				entity.setMaLoaiSua(result.getString("Ma_loai_sua"));
				entity.setTrongLuong(result.getInt("Trong_luong"));
				entity.setDonGia(result.getInt("Don_gia"));
				entity.setTpDinhDuong(result.getString("TP_Dinh_Duong"));
				entity.setLoiIch(result.getString("Loi_ich"));
				entity.setHinh(result.getString("Hinh"));
			}
			return entity;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}

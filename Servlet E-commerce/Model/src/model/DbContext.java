package model;

import lib.SQLConnectLib;

public class DbContext {
	private static final SQLConnectLib con = new SQLConnectLib("root",
			"",
			"jdbc:mysql://localhost:3306/ql_bansua?useUnicode=true&characterEncoding=UTF-8"
			);
	
	public static SQLConnectLib getInstance() {
		return con;
	}
}

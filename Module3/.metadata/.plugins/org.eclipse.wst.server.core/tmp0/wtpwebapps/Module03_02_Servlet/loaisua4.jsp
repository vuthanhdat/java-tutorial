<%-- 
    Document   : loai-sua-4
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Trang loại sữa 4</title>
    </head>
    <body>
        <form name="frmLoaiSua" action="loaisua" method="post">
            <table border="0">
                <caption>CẬP NHẬT LOẠI SỮA</caption>
                <tbody>
                    <tr>
                        <td>Mã loại</td>
                        <td><input type="text" name="txtMaLoai" value="${loaiSua.maLoaiSua}" /></td>
                    </tr>
                    <tr>
                        <td>Tên loại</td>
                        <td><input type="text" name="txtTenLoai" value="${loaiSua.tenLoaiSua}" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" value="Thêm" name="btnChon" />
                            <input type="submit" value="Tìm" name="btnChon" />
                            <input type="submit" value="Sửa" name="btnChon" />
                            <input type="submit" value="Xóa" name="btnChon" />
                        </td>
                    </tr>
                </tbody>
            </table>

        </form>
        <table border="1">
            <caption>DANH SÁCH LOẠI SỮA</caption>
            <thead>
                <tr>
                    <th>Mã loại</th>
                    <th>Tên loại</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${dsls}" var="ls">
                    <tr>
                        <td>${ls.maLoaiSua}</td>
                        <td>${ls.tenLoaiSua}</td>
                    </tr>
            </c:forEach>
            </tbody>
        </table>

    </body>
</html>

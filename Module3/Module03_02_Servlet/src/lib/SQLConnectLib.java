/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CoderNator
 * @category Database connection
 */
public class SQLConnectLib {
	public String dbUsername;
	public String dbPwd;
	public String dbUrl;
	private String dbType;
	public Connection conn;
	public Statement statement;
	public PreparedStatement preparedStatement;

	// -----------------------------------------------------------------------
	// TODO: BEGIN @Constructor

	// TODO: Default Constructor without parameter
	public SQLConnectLib() {
	}

	// TODO: Constructor with Default is MySQL DBMS
	public SQLConnectLib(String dbUsername, String dbPwd, String dbUrl) {
		this.dbUsername = dbUsername;
		this.dbPwd = dbPwd;
		this.dbUrl = dbUrl;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(dbUrl, dbUsername, dbPwd);
			statement = conn.createStatement();
		} catch (ClassNotFoundException | SQLException ex) {
			Logger.getLogger(SQLConnectLib.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	// TODO: Constructor with dbType will declare type of DBMS(Ex:MySQL, MariaDB,
	// SQL Server... );
	public SQLConnectLib(String dbUsername, String dbPwd, String dbType, String dbUrl) {
		this.dbUsername = dbUsername;
		this.dbPwd = dbPwd;
		this.dbUrl = dbUrl;
		switch (dbType) {
		case "mysql":
			// TODO: MySQL DBMS Driver String
			this.dbType = "com.mysql.jdbc.Driver";
			break;
		case "mariadb":
			// TODO: Use MariaDB DBMS Driver String
			this.dbType = "org.mariadb.jdbc.Driver";
			break;
		case "sqlserver":
			// TODO: Use SQL Server DBMS Driver String
			this.dbType = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			break;
		case "sqlserver_mod":
			// TODO: Use SQL Server DBMS Driver of 3rd Party String
			this.dbType = "net.sourceforge.jtds.jdbc.Driver";
			break;
		default:
			break;
		}
		try {
			Class.forName(this.dbType);
			conn = DriverManager.getConnection(dbUrl, dbUsername, dbPwd);
			statement = conn.createStatement();
		} catch (ClassNotFoundException | SQLException ex) {
			Logger.getLogger(SQLConnectLib.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	// END Constructor
	// -----------------------------------------------------------------------

	// -----------------------------------------------------------------------
	// TODO: BEGIN Getter & Setter
	public String getDbUsername() {
		return dbUsername;
	}

	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}

	public String getDbPwd() {
		return dbPwd;
	}

	public void setDbPwd(String dbPwd) {
		this.dbPwd = dbPwd;
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public Statement getStatement() {
		return statement;
	}

	public void setStatement(Statement statement) {
		this.statement = statement;
	}
	// END Getter & Setter
	// -----------------------------------------------------------------------

	// TODO: return Prepared Statement
	public PreparedStatement returnPrepareStatement(String query) {
		try {
			preparedStatement = conn.prepareStatement(query);
		} catch (SQLException ex) {
			Logger.getLogger(SQLConnectLib.class.getName()).log(Level.SEVERE, null, ex);
		}
		return preparedStatement;
	}

	// TODO: Execute query by Statement (Query type: SELECT);
	public ResultSet execQuery(String query) {
		ResultSet result;
		try {
			result = statement.executeQuery(query);
			return result;
		} catch (SQLException ex) {
			Logger.getLogger(SQLConnectLib.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	// TODO: Execute query by Statement (Query type: UPDATE, INSERT, DELETE);
	public int execUpdateQuery(String query) {
		int result = -1;
		try {
			result = statement.executeUpdate(query);
			return result;
		} catch (SQLException ex) {
			Logger.getLogger(SQLConnectLib.class.getName()).log(Level.SEVERE, null, ex);
		}
		return result;
	}

	// TODO: Execute SELECT query by Insert TABLE NAME;
	public ResultSet execSELECTQuery(String tableName) {
		ResultSet result = null;
		try {
			result = statement.executeQuery("SELECT * FROM " + tableName + ";");
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public int execCOUNTQuery(String tableName) {
		int row = -1;
		ResultSet result = null;
		try {
			result = statement.executeQuery("SELECT COUNT(*) as Rcount FROM " + tableName + ";");
			result.next();
			row = result.getInt("Rcount");
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return row;
	}

	public ResultSet execSELECTandWHEREQueryWithSingleStatement(String tableName, String fieldName, String value) {
		ResultSet result = null;
		String query = "SELECT * FROM " + tableName + " WHERE "+fieldName+" = '"+value+"';";
		try {
			result = statement.executeQuery(query);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	//TODO: Execute single query with WHERE and LIKE 
	public ResultSet execSELECTandWHEREandLIKE(String tableName, String fieldName, String value) {
		ResultSet result = null;
		String query = "SELECT * FROM " + tableName + " WHERE "+fieldName+" = '%"+value+"%';";
		try {
			result = statement.executeQuery(query);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	public int countByResultSet(ResultSet resultSet) {
		int totalRow = -1;
		try {
			resultSet.last();
			totalRow = resultSet.getRow();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return totalRow;
	}
}

package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javabeans.LoaiSua;
import model.LoaiSuaBL;

/**
 * Servlet implementation class LoaiSuaServlet
 */
@WebServlet({ "/LoaiSuaServlet", "/loaisua", "/LoaiSua" })
public class LoaiSuaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private LoaiSuaBL bl;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoaiSuaServlet() {
		super();
		// TODO Auto-generated constructor stub
		bl = new LoaiSuaBL();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.sendRedirect("vd2_themloaisua_Servlet.jsp");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");

		String maLoai = "", tenLoai = "",action;
		int result = -1;
		List<LoaiSua> dsls = null;
		LoaiSua entity = null;
		action = request.getParameter("btnChon");
		
		if (action != null) {
			maLoai = request.getParameter("txtMaLoai");
			tenLoai = request.getParameter("txtTenLoai");
			
			entity = new LoaiSua(maLoai, tenLoai);
			switch (action) {
			case "Thêm":
				result = new LoaiSuaBL().addItem(entity);
				break;
			case "Sửa":
				result = new LoaiSuaBL().updateItem(entity);
				break;
			case "Xóa":
				result = new LoaiSuaBL().deleteRow(maLoai);
				break;
			}
		}
		dsls = bl.getAll();
		request.setAttribute("loaiSua", entity);
		request.setAttribute("dsls", dsls);
		request.getRequestDispatcher("/loaisua4.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);

	}

}

package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lib.SQLConnectLib;

/**
 * Servlet implementation class BT2_Servlet
 */
@WebServlet("/BT2_Servlet")
public class BT2_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BT2_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();

		// HTML5
		out.println("<html>" + "<head>" +"<meta charset='UTF-8'>"
				+ "<title>Thông tin khách hàng</title>" + "</head>" + "<body>");
		out.println("<h1 align='center'>Thông tin khách hàng</h1>");
		out.println("<table border='2'>");
		out.println("<tr>" + "<td>Mã KH</td>" + "<td>Tên KH</td>"
				+ "<td>Giới tính</td>" + "<td>�?ịa chỉ</td>" + "<td>Sđt</td>"
				+ "</tr>");

		SQLConnectLib lib = new SQLConnectLib(
				"root",
				"root",
				"jdbc:mysql://localhost:3306/ql_bansua?useUnicode=true&amp;characterEncoding=UTF-8");
		String query = "SELECT * FROM khach_hang;";
		PreparedStatement preparedStatement = lib.returnPrepareStatement(query);
		ResultSet result;
		try {
			result = preparedStatement.executeQuery();
			while (result.next()) {
				out.println("<tr>");
				out.println("<td>"+result.getString("Ma_khach_hang")+"</td>");
				out.println("<td>"+result.getString("Ten_khach_hang")+"</td>");
				if(result.getBoolean("Phai")){
					out.println("<td><img src='public/images/nu.jpg'></td>");
				}
				else{
					out.println("<td><img src='public/images/nam.jpg'></td>");
				}
				out.println("<td>"+result.getString("Dia_chi")+"</td>");
				out.println("<td>"+result.getString("Dien_thoai")+"</td>");
				out.println("</tr>");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//END HTML
		out.println("</table>" +
				"</body>"+
				"</html>"
				);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

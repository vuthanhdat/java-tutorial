package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javabeans.Sanpham;
import lib.SQLConnectLib;

public class SanPhamBL {
	private SQLConnectLib dbc;

	public SanPhamBL() {
		dbc = DbContext.getInstance();
	}

	public List<Sanpham> getAll() {
		List<Sanpham> sanphams = new ArrayList<Sanpham>();
		PreparedStatement preparedStatement = dbc.returnPrepareStatement("SELECT * FROM sua;");
		ResultSet result;
		try {
			result = preparedStatement.executeQuery();
			sanphams =  parseFromResultSet(result);
			return sanphams;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return sanphams;
	}

	public List<Sanpham> findItem(String name) {
		List<Sanpham> sanphams = new ArrayList<Sanpham>();
		PreparedStatement preparedStatement = dbc.returnPrepareStatement("SELECT * FROM sua WHERE Ten_sua LIKE ?;");
		ResultSet result;
		try {
			preparedStatement.setString(1, "%" + name + "%");
			result = preparedStatement.executeQuery();
			sanphams =  parseFromResultSet(result);
			return sanphams;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return sanphams;
	}

	public Sanpham getById(String id) {
		Sanpham sanpham = new Sanpham();
		PreparedStatement preparedStatement = dbc.returnPrepareStatement("SELECT * FROM sua WHERE Ma_sua = ?;");
		ResultSet result;
		try {
			preparedStatement.setString(1, id);
			result = preparedStatement.executeQuery();
			sanpham = this.parseFromResultSet1Row(result);
			return sanpham;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public Sanpham getById_02(String id) {
		Sanpham sanpham = new Sanpham();
		ResultSet result;
		try {
			result = dbc.execSELECTandWHEREQueryWithSingleStatement("sua", "Ma_sua", id);
			sanpham = this.parseFromResultSet1Row(result);
			return sanpham;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public int getRowCount() {
		return dbc.execCOUNTQuery("sua");
	}

	private List<Sanpham> parseFromResultSet(ResultSet result) {
		List<Sanpham> listOfSanPham = new ArrayList<Sanpham>();
		try {
			while (result.next()) {
				Sanpham entity = new Sanpham();
				entity.setMaSua(result.getString("Ma_sua"));
				entity.setTenSua(result.getString("Ten_sua"));
				entity.setMaHangSua(result.getString("Ma_hang_sua"));
				entity.setMaLoaiSua(result.getString("Ma_loai_sua"));
				entity.setTrongLuong(result.getInt("Trong_luong"));
				entity.setDonGia(result.getInt("Don_gia"));
				entity.setTpDinhDuong(result.getString("TP_Dinh_Duong"));
				entity.setLoiIch(result.getString("Loi_ich"));
				entity.setHinh(result.getString("Hinh"));
				listOfSanPham.add(entity);
			}
			return listOfSanPham;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Sanpham parseFromResultSet1Row(ResultSet result) {
		Sanpham entity = new Sanpham();
		try {
			while (result.next()) {
				//Sanpham entity = new Sanpham();
				entity.setMaSua(result.getString("Ma_sua"));
				entity.setTenSua(result.getString("Ten_sua"));
				entity.setMaHangSua(result.getString("Ma_hang_sua"));
				entity.setMaLoaiSua(result.getString("Ma_loai_sua"));
				entity.setTrongLuong(result.getInt("Trong_luong"));
				entity.setDonGia(result.getInt("Don_gia"));
				entity.setTpDinhDuong(result.getString("TP_Dinh_Duong"));
				entity.setLoiIch(result.getString("Loi_ich"));
				entity.setHinh(result.getString("Hinh"));
			}
			return entity;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}

package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javabeans.LoaiSua;
import lib.SQLConnectLib;

public class LoaiSuaBL {
	SQLConnectLib dbc = DbContext.getInstance();

	public LoaiSuaBL() {

	}

	public List<LoaiSua> getAll() {
		List<LoaiSua> loais = new ArrayList<LoaiSua>();
		PreparedStatement preparedStatement = dbc.returnPrepareStatement("SELECT * FROM loai_sua;");
		ResultSet result;
		try {
			result = preparedStatement.executeQuery();
			loais = parseFromResult(result);
			return loais;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return loais;
	}
	
	public int updateItem(LoaiSua entity) {
		int result = -1;
		String query = "UPDATE `ql_bansua`.`loai_sua` SET `Ten_loai` = ? WHERE `loai_sua`.`Ma_loai_sua` = ?;";
		PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);

		try {
			preparedStatement.setString(1, entity.getTenLoaiSua());
			preparedStatement.setString(2, entity.getMaLoaiSua());
			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public int deleteRow(String id) {
		int result = -1;
		String query = "DELETE FROM `ql_bansua`.`loai_sua` WHERE `loai_sua`.`Ma_loai_sua` = ? ;";
		PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);

		try {
			preparedStatement.setString(1, id);
			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	public int addItem(LoaiSua entity) {
		int result = -1;
		String query = "INSERT INTO `ql_bansua`.`loai_sua` (`Ma_loai_sua`, `Ten_loai`) VALUES (?, ?);";
		PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);

		try {
			preparedStatement.setString(1, entity.getMaLoaiSua());
			preparedStatement.setString(2, entity.getTenLoaiSua());
			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	private List<LoaiSua> parseFromResult(ResultSet result){
		List<LoaiSua> listOfLoaisua = new ArrayList<LoaiSua>();
		try {
			while (result.next()) {
				LoaiSua entity = new LoaiSua();
				entity.setMaLoaiSua(result.getString("Ma_loai_sua"));
				entity.setTenLoaiSua(result.getString("Ten_loai"));
				listOfLoaisua.add(entity);
			}
			return listOfLoaisua;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	private LoaiSua parseRowFromResult(ResultSet result){
		LoaiSua entity = new LoaiSua();
		try {
			while (result.next()) {
				entity = new LoaiSua();
				entity.setMaLoaiSua(result.getString("Ma_loai_sua"));
				entity.setTenLoaiSua(result.getString("Ten_loai"));
			}
			return entity;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
}

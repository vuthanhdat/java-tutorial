<%@page import="entities.LoaiSua"%>
<%@page import="model.LoaiSuaBL"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Type</title>
</head>
<%
	int result=-2;
	String maLoai = "", tenLoai = "";
	if (request.getParameter("btnAdd") != null) {
		maLoai=request.getParameter("txtMaLoai");
		tenLoai=request.getParameter("txtTenLoai");
		LoaiSua entity = new LoaiSua(maLoai,tenLoai);
		result= new LoaiSuaBL().addItem(entity);
	}
%>
<body>
	<form action="/themloaisua" method="post">
		<table>
			<tr>
				<td>Mã loại</td>
				<td><input type="text" name="txtMaLoai" value="<%=maLoai%>"></td>
			</tr>
			<tr>
				<td>Tên loại</td>
				<td><input type="text" name="txtTenLoai" value="<%=tenLoai%>"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="btnAdd" value="Add"></td>
			</tr>
		</table>
	</form>
	<h2><%if(result>0){
		out.println("Added entity to Database successfully!");
	}
	else if(result!=-2) {
		out.println("Error occured when adding into Db!");
	}
	%></h2>
</body>
</html>
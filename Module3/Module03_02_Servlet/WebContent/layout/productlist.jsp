<%@page import="javafx.scene.control.Pagination"%>
<%@page import="java.util.List"%>
<%@page import="javabeans.Sanpham"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.SanPhamBL"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%!SanPhamBL bl = new SanPhamBL();
	List<Sanpham> entities = bl.getAll();
	Pagination pagination;
	String txtTenSP = "";
	int records = -1;%>
<%
	if (request.getParameter("txtTenSP") != null) {
		txtTenSP = request.getParameter("txtTenSP");
		entities = bl.findItem(txtTenSP);
		if (entities.size() > 0) {
			records = entities.size();
		} else {
			records = -1;
		}
	}
	if (records > 0) {
		out.print("<h1>Result get :" + records + "</h1>");
	}
%>
<div class="row">
	<%
		for (Sanpham sp : entities) {
	%>
	<div class="col-lg-4 col-md-6 mb-4">
		<div class="card h-100">
			<a href="#"><img class="card-img-top"
				src="./public/images/<%=sp.hinh%>" alt=""></a>
			<div class="card-body">
				<h4 class="card-title">
					<a href="baitap06.jsp?id=<%=sp.maSua%>"><%=sp.tenSua%></a>
				</h4>
				<h5><%=sp.donGia%>
					VND
				</h5>
				<p class="card-text"><%=sp.tpDinhDuong%></p>
			</div>
			<div class="card-footer">
				<small class="text-muted">&#9733; &#9733; &#9733; &#9733;
					&#9734;</small>
			</div>
		</div>
	</div>

	<%
		}
	%>
</div>
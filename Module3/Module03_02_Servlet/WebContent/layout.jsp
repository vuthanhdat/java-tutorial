<%@page import="lib.Pagination"%>
<%@page import="entities.Sanpham"%>
<%@page import="java.util.List"%>
<%@page import="model.SanPhamBL"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<!-- Head tag -->
<%@include file="./layout/head.html"%>
<!-- End Head tag -->

<body>
	<!-- Navigation -->
	<jsp:include page="./layout/navbar.jsp"></jsp:include>
	<!-- End Navigation -->
	<!-- Page Content -->
	<div class="container">
		<div class="row">
			<!-- Product Category -->
			<jsp:include page="./layout/category.jsp"></jsp:include>
			<!-- /.col-lg-3 -->

			<div class="col-lg-9">

				<!-- Advertisement -->
				<jsp:include page="./layout/advertise.jsp"></jsp:include>
				
				<!-- row -->
				<jsp:include page="./layout/productlist.jsp"></jsp:include>
				<!-- /.row -->

			</div>
			<!-- /.col-lg-9 -->

		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->

	<!-- Footer -->
	<jsp:include page="./layout/footer.jsp"></jsp:include>

	<!-- Bootstrap core JavaScript -->


</body>

</html>

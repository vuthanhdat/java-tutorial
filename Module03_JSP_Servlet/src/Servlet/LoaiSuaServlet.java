package Servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entities.LoaiSua;
import Model.LoaiSuaBL;

/**
 * Servlet implementation class LoaiSuaServlet
 */
@WebServlet({"/LoaiSuaServlet","/themloaisua"})
public class LoaiSuaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoaiSuaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("vd2_themloaisua_Servlet.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String maLoai = "", tenLoai = "";
		int result=-1;
		if (request.getParameter("btnAdd") != null) {
			maLoai=request.getParameter("txtMaLoai");
			tenLoai=request.getParameter("txtTenLoai");
			LoaiSua entity = new LoaiSua(maLoai,tenLoai);
			result= new LoaiSuaBL().addItem(entity);
		}
		if(result>0) {
			request.getRequestDispatcher("vd2_themloaisua_Servlet.jsp").forward(request, response);
			request.setAttribute("kq", "Added entity to Database successfully!");
		}
	}

}

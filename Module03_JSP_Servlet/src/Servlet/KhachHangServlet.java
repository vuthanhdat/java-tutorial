package Servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entities.KhachHang;
import Model.KhachHangBL;

/**
 * Servlet implementation class KhachHang
 */
@WebServlet("/KhachHangServlet")
public class KhachHangServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KhachHangServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.sendRedirect("themKhachHang.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int result=-1;
		String txtMakh = "", txtTenKh = "", txtGioiTinh = "", txtDiaChi = "", txtEmail = "", txtDT = "";
		if (request.getParameter("submit") != null) {
			txtMakh = request.getParameter("txtMaKH");
			txtTenKh = request.getParameter("txtTenKH");
			txtGioiTinh = request.getParameter("txtGioiTinh");
			int gender = Integer.parseInt(txtGioiTinh);
			boolean bgender = gender==0?true:false;
			txtDT = request.getParameter("txtSDT");
			txtDiaChi = request.getParameter("txtDiachi");
			txtEmail = request.getParameter("txtEmail");
			KhachHang entity = new KhachHang(txtMakh, txtTenKh, bgender, txtDiaChi, txtDT, txtEmail);
			result = new KhachHangBL().insertRow(entity);
		}
		if(result>0) {
			request.getRequestDispatcher("themKhachHang.jsp").forward(request, response);
		}
	}

}

package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entities.HangSua;
import Lib.SQLConnectLib;

public class HangSuaBL {
	SQLConnectLib dbc;
	public HangSuaBL() {
		dbc = DbContext.getInstance();
	}
	
	public List<HangSua> getAll(){
		List<HangSua> listOfHangSua = new ArrayList<HangSua>();
		return listOfHangSua;
	}
	
	public HangSua getById(String id) {
		HangSua item = new HangSua();
		ResultSet result = dbc.execSELECTandWHEREQueryWithSingleStatement("hang_sua", "Ma_hang_sua", id);
		try {
			result.next();
			item.maHangSua = result.getString("Ma_hang_sua");
			item.tenHangSua= result.getString("Ten_hang_sua");
			item.diaChi = result.getString("Dia_chi");
			item.dienThoai = result.getString("Dien_thoai");
			item.email = result.getString("Email");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return item;
	}
}

package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Entities.Sanpham;
import Lib.SQLConnectLib;

public class SanPhamBL {
	private SQLConnectLib dbc;

	public SanPhamBL() {
		dbc = DbContext.getInstance();
	}

	public List<Sanpham> getAll() {
		List<Sanpham> sanphams = new ArrayList<Sanpham>();
		PreparedStatement preparedStatement = dbc.returnPrepareStatement("SELECT * FROM sua;");
		ResultSet result;
		try {
			result = preparedStatement.executeQuery();
			while (result.next()) {
				Sanpham entity = new Sanpham();
				entity.maSua = result.getString("Ma_sua");
				entity.tenSua = result.getString("Ten_sua");
				entity.maHangSua = result.getString("Ma_hang_sua");
				entity.maLoaiSua = result.getString("Ma_loai_sua");
				entity.trongLuong = result.getInt("Trong_luong");
				entity.donGia = result.getInt("Don_gia");
				entity.tpDinhDuong = result.getString("TP_Dinh_Duong");
				entity.loiIch = result.getString("Loi_ich");
				entity.hinh = result.getString("Hinh");
				sanphams.add(entity);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return sanphams;
	}
	
	public List<Sanpham> findItem(String name) {
		List<Sanpham> sanphams = new ArrayList<Sanpham>();
		PreparedStatement preparedStatement = dbc.returnPrepareStatement("SELECT * FROM sua WHERE Ten_sua LIKE ?;");
		ResultSet result;
		try {
			preparedStatement.setString(1, "%"+name+"%");
			result = preparedStatement.executeQuery();
			while (result.next()) {
				Sanpham entity = new Sanpham();
				entity.maSua = result.getString("Ma_sua");
				entity.tenSua = result.getString("Ten_sua");
				entity.maHangSua = result.getString("Ma_hang_sua");
				entity.maLoaiSua = result.getString("Ma_loai_sua");
				entity.trongLuong = result.getInt("Trong_luong");
				entity.donGia = result.getInt("Don_gia");
				entity.tpDinhDuong = result.getString("TP_Dinh_Duong");
				entity.loiIch = result.getString("Loi_ich");
				entity.hinh = result.getString("Hinh");
				sanphams.add(entity);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return sanphams;
	}
	
	public Sanpham getById(String id) {
		Sanpham sanpham = new Sanpham();
		PreparedStatement preparedStatement = dbc.returnPrepareStatement("SELECT * FROM sua WHERE Ma_sua = ?;");
		ResultSet result;
		try {
			preparedStatement.setString(1, id);
			result = preparedStatement.executeQuery();
			while (result.next()) {
				sanpham.maSua = result.getString("Ma_sua");
				sanpham.tenSua = result.getString("Ten_sua");
				sanpham.maHangSua = result.getString("Ma_hang_sua");
				sanpham.maLoaiSua = result.getString("Ma_loai_sua");
				sanpham.trongLuong = result.getInt("Trong_luong");
				sanpham.donGia = result.getInt("Don_gia");
				sanpham.tpDinhDuong = result.getString("TP_Dinh_Duong");
				sanpham.loiIch = result.getString("Loi_ich");
				sanpham.hinh = result.getString("Hinh");
			}
			return sanpham;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	public Sanpham getById_02(String id) {
		Sanpham sanpham = new Sanpham();
		ResultSet result;
		try {
			result = dbc.execSELECTandWHEREQueryWithSingleStatement("sua", "Ma_sua", id);
			while (result.next()) {
				sanpham.maSua = result.getString("Ma_sua");
				sanpham.tenSua = result.getString("Ten_sua");
				sanpham.maHangSua = result.getString("Ma_hang_sua");
				sanpham.maLoaiSua = result.getString("Ma_loai_sua");
				sanpham.trongLuong = result.getInt("Trong_luong");
				sanpham.donGia = result.getInt("Don_gia");
				sanpham.tpDinhDuong = result.getString("TP_Dinh_Duong");
				sanpham.loiIch = result.getString("Loi_ich");
				sanpham.hinh = result.getString("Hinh");
			}
			return sanpham;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	public int getRowCount() {
		return dbc.execCOUNTQuery("sua");
	}
}

package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entities.LoaiSua;
import Entities.Sanpham;
import Lib.SQLConnectLib;

public class LoaiSuaBL {
	SQLConnectLib dbc = DbContext.getInstance();
	
	public LoaiSuaBL() {
		
	}
	
	public List<LoaiSua> getAll(){
		List<LoaiSua> loais = new ArrayList<LoaiSua>();
		PreparedStatement preparedStatement = dbc.returnPrepareStatement("SELECT * FROM loai_sua;");
		ResultSet result;
		try {
			result = preparedStatement.executeQuery();
			while(result.next()) {
				LoaiSua entity = new LoaiSua();
						entity.maLoaiSua= result.getString("Ma_loai_sua"); 
						entity.tenLoaiSua =result.getString("Ten_loai"); 
						loais.add(entity);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return loais;
	}
	
	public int addItem(LoaiSua entity) {
		int result =-1;
		String query="INSERT INTO `ql_bansua`.`loai_sua` (`Ma_loai_sua`, `Ten_loai`) VALUES (?, ?);";
		PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);
		
		try {
			preparedStatement.setString(1, entity.maLoaiSua);
			preparedStatement.setString(2, entity.tenLoaiSua);
			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
}

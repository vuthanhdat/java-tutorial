package Model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import Entities.KhachHang;
import Lib.SQLConnectLib;

public class KhachHangBL {
	SQLConnectLib dbc;

	public KhachHangBL() {
		this.dbc = DbContext.getInstance();
	}

	////`Ma_khach_hang`, `Ten_khach_hang`, `Phai`, `Dia_chi`, `Dien_thoai`, `Email`
	public int insertRow(KhachHang entity) {
		int result = -1;
		String query = "INSERT INTO khach_hang (`Ma_khach_hang`, `Ten_khach_hang`, `Phai`, `Dia_chi`, `Dien_thoai`, `Email`) "
				+ " VALUES(?, ?, ?, ?, ?, ?);";
		PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);
		try {
			preparedStatement.setString(1, entity.maKhachHang);
			preparedStatement.setString(2, entity.tenKhachHang);
			preparedStatement.setBoolean(3, entity.phai);
			preparedStatement.setString(4, entity.diaChi);
			preparedStatement.setString(5, entity.dienThoai);
			preparedStatement.setString(6, entity.email);
			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
}

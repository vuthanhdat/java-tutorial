package Entities;

import java.io.Serializable;

public class LoaiSua implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String maLoaiSua;
	public String tenLoaiSua;
	
	public LoaiSua() {
		
	}
	
	public LoaiSua(String maLoaiSua, String tenLoaiSua) {
		super();
		this.maLoaiSua = maLoaiSua;
		this.tenLoaiSua = tenLoaiSua;
	}

	public String getMaLoaiSua() {
		return maLoaiSua;
	}

	public void setMaLoaiSua(String maLoaiSua) {
		this.maLoaiSua = maLoaiSua;
	}

	public String getTenLoaiSua() {
		return tenLoaiSua;
	}

	public void setTenLoaiSua(String tenLoaiSua) {
		this.tenLoaiSua = tenLoaiSua;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}

<%@page import="Model.LoaiSuaBL"%>
<%@page import="Entities.LoaiSua"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./public/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<script src="./public/vendor/jquery/jquery.min.js"></script>
<script src="./public/vendor/popper/popper.min.js"></script>
<script src="./public/vendor/bootstrap/js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<%!LoaiSuaBL bl = new LoaiSuaBL();
	List<LoaiSua> loai = bl.getAll();%>

<body>
	<div class="container">
		<table class="table">
			<tr>
				<th>Loại sữa</th>
				<th>Tên loại</th>
			</tr>
			<%
				for (LoaiSua l : loai) {
			%>

			<tr>
				<td><%=l.maLoaiSua%></td>
				<td><%=l.tenLoaiSua%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
</body>
</html>
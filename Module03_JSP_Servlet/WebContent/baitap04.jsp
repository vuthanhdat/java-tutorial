<%@page import="java.util.Locale"%>
<%@page import="Entities.Sanpham"%>
<%@page import="java.util.List"%>
<%@page import="Model.SanPhamBL"%>
<%@page import="java.text.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./public/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<script src="./public/vendor/jquery/jquery.min.js"></script>
<script src="./public/vendor/popper/popper.min.js"></script>
<script src="./public/vendor/bootstrap/js/bootstrap.min.js"></script>

<title>Insert title here</title>
<%!SanPhamBL bl = new SanPhamBL();
	List<Sanpham> entities = bl.getAll();
	Locale lc = new Locale("vi", "VN");
	NumberFormat nf = NumberFormat.getInstance(lc);
	int col;%>
</head>
<body>
	<div class="container">
		<table class="table">

			<%
			col=1;
				for (Sanpham item : entities) {
			%>
			<%
				if (col++==1) {
			%>
			<tr>
				<%
					}
				%>
				<td>
					<p>
						<a href="baitap06.jsp?id=<%=item.maSua%>"><b><%=item.tenSua%></b></a>
					</p>
					<p><%=item.trongLuong%> gr - 
						<%=nf.format(item.donGia)%>
						VNĐ
					</p> <img src="./public/images/<%=item.hinh%>">
				</td>
				<%
					if (col == 6) {
							col = 1;
				%>
			</tr>
			<%
				}
			%>
			<%
				}
			%>
			<%
				if (col != 1) {
			%>
			</tr>
			<%
				}
			%>
		</table>
	</div>
</body>
</html>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="Lib.SQLConnectLib"%>
<%@page import="Model.DbContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./public/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<script src="./public/vendor/jquery/jquery.min.js"></script>
<script src="./public/vendor/popper/popper.min.js"></script>
<script src="./public/vendor/bootstrap/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insert title here</title>
<%!SQLConnectLib dbc = DbContext.getInstance();
	PreparedStatement preparedStatement = dbc.returnPrepareStatement(
			"SELECT sua.`Ten_sua`,`Ten_hang_sua`,loai_sua.`Ten_loai`, sua.`Trong_luong`, `Don_gia`,`Hinh` "
					+ " FROM `sua`" + " JOIN `loai_sua` ON loai_sua.Ma_loai_sua = sua.Ma_loai_sua"
					+ " JOIN `hang_sua` ON sua.Ma_hang_sua = hang_sua.Ma_hang_sua");
	ResultSet result;%>
</head>
<body>
	<%
		result = preparedStatement.executeQuery();
	%>

	<div class="container">
	<h1>THÔNG TIN HÃNG SỮA</h1>
		<table class="table table-hover">
		<caption>THÔNG TIN HÃNG SỮA</caption>
			<%
				while (result.next()) {
			%>

			<tr>
				<td><img alt=""
					src="./public/images/<%=result.getString("Hinh")%>"></td>
				<td>
					<p>
						<b><%=result.getString("Ten_sua")%></b>
					</p>
					<p>
						Nhà sản xuất:
						<%=result.getString("Ten_hang_sua")%>
					</p>
					<p><%=result.getString("Ten_loai")%>
						-
						<%=result.getInt("Trong_luong")%>
						G -
						<%=result.getInt("Don_gia")%>
						VNĐ
					</p>
				</td>


			</tr>
			<%
				}
			%>
		</table>
	</div>
</body>
</html>
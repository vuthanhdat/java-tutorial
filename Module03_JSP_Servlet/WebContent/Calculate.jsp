<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Calculate</title>
<%
	String num1 = "", num2 = "", result = "";
	int res = 0;
	if (request.getParameter("btnAdd") != null) {
		num1 = request.getParameter("txtNum1");
		num2 = request.getParameter("txtNum2");
		res = Integer.parseInt(num1) + Integer.parseInt(num2);
		result = String.valueOf(res);
	}

	if (request.getParameter("btnSub") != null) {
		num1 = request.getParameter("txtNum1");
		num2 = request.getParameter("txtNum2");
		res = Integer.parseInt(num1) - Integer.parseInt(num2);
		result = String.valueOf(res);
	}

	if (request.getParameter("btnMul") != null) {
		num1 = request.getParameter("txtNum1");
		num2 = request.getParameter("txtNum2");
		res = Integer.parseInt(num1) * Integer.parseInt(num2);
		result = String.valueOf(res);
	}

	if (request.getParameter("btnDiv") != null) {
		num1 = request.getParameter("txtNum1");
		num2 = request.getParameter("txtNum2");
		res = Integer.parseInt(num1) / Integer.parseInt(num2);
		result = String.valueOf(res);
	}
%>
</head>

<body>
	<form action="" method="post">
		<table>
			<tr>
				<td>Number 1</td>
				<td><input type="text" name="txtNum1" value="<%=num1%>"></td>
			</tr>
			<tr>
				<td>Number 2</td>
				<td><input type="text" name="txtNum2" value="<%=num2%>"></td>
			</tr>
			<tr>
				<td>Result</td>
				<td><input type="text" name="txtResult" value="<%=result%>"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="btnAdd" value="Add"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="btnSub"
					value="Subtract"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="btnMul"
					value="Multiple"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="btnDiv"
					value="Divide"></td>
			</tr>
		</table>
	</form>
</body>
</html>
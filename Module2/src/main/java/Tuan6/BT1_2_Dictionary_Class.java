/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan6;

/**
 *
 * @author BaoHa
 */
public class BT1_2_Dictionary_Class implements Comparable<BT1_2_Dictionary_Class>{
    String word;
    String meaning;

    public BT1_2_Dictionary_Class() {
    }

    public BT1_2_Dictionary_Class(String word, String meaning) {
        this.word = word;
        this.meaning = meaning;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    @Override
    public int compareTo(BT1_2_Dictionary_Class o) {
        return this.word.compareTo(o.word);
    }
    
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan6;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BaoHa
 */
public class PetManager<T> {
    private List<T> pets;

    public PetManager(){
        
    }
    public PetManager(List<T> pets) {
        this.pets = pets;
    }

    public List<T> getPets() {
        if(pets==null){
            pets = new ArrayList<>();
        }
        return this.pets;
    }

    public void setPets(List<T> pets) {
        this.pets = pets;
    }
    
    
}

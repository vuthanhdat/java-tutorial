/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan9_Proxy;

/**
 *
 * @author hv
 */
public class HinhAnhReal implements HinhAnh{

    private String hinhAnh;

    public HinhAnhReal(String hinhAnh) {
        this.hinhAnh = hinhAnh;
        taiHinh();
    }
    
    @Override
    public void xuat() {
        System.out.println("Get Image : "+ hinhAnh);
    }
    
    public void taiHinh(){
        System.out.println("Image Uploaded : "+ hinhAnh);
    }
}

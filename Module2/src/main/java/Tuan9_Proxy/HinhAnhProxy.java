/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan9_Proxy;

/**
 *
 * @author hv
 */
public class HinhAnhProxy implements HinhAnh{
    private HinhAnhReal hinhAnhReal = null;
    private String hinhAnh;
    public HinhAnhProxy(String hinhAnh){
        this.hinhAnh = hinhAnh;
    }
    
    @Override
    public void xuat(){
        if(hinhAnhReal==null){
            hinhAnhReal = new HinhAnhReal(hinhAnh);
        }
        hinhAnhReal.xuat();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lib;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author BaoHa
 */
public class MD5Converter {
    public static String convert(String input){
        String md5 =null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(input.getBytes());
            byte[] mb = md.digest();
            md5 = DatatypeConverter.printHexBinary(mb);
            return md5;
            //System.out.println(m);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(MD5Converter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return md5;
    }
}

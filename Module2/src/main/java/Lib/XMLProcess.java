/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BaoHa
 */
public class XMLProcess {
   public static String parseJSON(String link){
       try {
           URL url = new URL(link);
           URLConnection urlCon = url.openConnection();
           BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
           String line = "";
           if((line = br.readLine())!=null){
               line = br.readLine();
           }
           return line;
       } catch (MalformedURLException ex) {
           Logger.getLogger(XMLProcess.class.getName()).log(Level.SEVERE, null, ex);
       } catch (IOException ex) {
           Logger.getLogger(XMLProcess.class.getName()).log(Level.SEVERE, null, ex);
       }
       return null;
   }
   
   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan7;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author BaoHa
 */
public class BT3_1_Lambda_Demo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        List<Integer> list = Arrays.asList(1,2,3,4,5,6,7);
        list.forEach(x->{System.out.println(x);});
    }
    
}

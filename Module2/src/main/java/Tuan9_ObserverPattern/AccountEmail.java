/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan9_ObserverPattern;

/**
 *
 * @author hv
 */
public class AccountEmail extends AccountObserver{
    public AccountEmail(AccountObservable account){
        this.account = account;
        this.account.addObserver(this);
    }
    
    @Override
    public void notice(String notification) {
        System.out.println("Email: "+ notification);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan9_ObserverPattern;

/**
 *
 * @author hv
 */
public class TinNhan extends AccountObserver{
    public TinNhan(AccountObservable account){
        this.account = account;
        this.account.addObserver(this);
    }
    
    @Override
    public void notice(String notification) {
        System.out.println("SMS: "+ notification);
    }
}

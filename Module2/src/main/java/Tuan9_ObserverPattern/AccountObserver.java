/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan9_ObserverPattern;

/**
 *
 * @author hv
 */
public abstract class AccountObserver {
    protected AccountObservable account;
    public abstract void notice(String notification);
}

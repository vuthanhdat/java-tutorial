package Tuan9_ObserverPattern;

/**
 *
 * @author hv
 */
public class UnitTestTuan9_Observer {

    public static void main(String[] args) {
        AccountObservable acc = new AccountObservable();
        TinNhan tn = new TinNhan(acc);
        AccountEmail email = new AccountEmail(acc);
        acc.setCash(10000);
    }

}

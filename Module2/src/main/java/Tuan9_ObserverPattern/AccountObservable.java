/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan9_ObserverPattern;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hv
 */
public class AccountObservable {
    List<AccountObserver> listObserver = new ArrayList<>();
    private int cash;
    public int getCash(){
        return cash;
    }
    
    public void setCash(int cash){
        this.cash += cash;
        this.noticeObserver(cash);
    }
    
    public void noticeObserver(int money){
        for(AccountObserver o:listObserver){
            o.notice("Cash has been changed:" + money);
        }
    }
    
    public void addObserver(AccountObserver accountObserver){
        listObserver.add(accountObserver);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan9_Decorator;

/**
 *
 * @author hv
 */
public abstract class Decorator implements Hinh{

    Hinh hinh;
    protected Decorator(Hinh hinh){
        this.hinh = hinh; 
    }
    @Override
    public void xuat() {
        hinh.xuat();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan9_Decorator;

/**
 *
 * @author hv
 */
public class VienDo extends Decorator{
    
    public VienDo(Hinh hinh) {
        super(hinh);
    }
    
    @Override
    public void xuat(){
        super.xuat();
        System.out.println("Viền đỏ");
    }
}

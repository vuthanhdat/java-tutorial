/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan9_Decorator;

/**
 *
 * @author hv
 */
public class DesignPattern_Example_Decorator {
    public static void main(String[] args) {
        //Hinh h = new HinhTronDVD();
        //h.xuat();
        Hinh h = new HinhTron();
        VienDo vd = new VienDo(h);
        vd.xuat();
    }
}

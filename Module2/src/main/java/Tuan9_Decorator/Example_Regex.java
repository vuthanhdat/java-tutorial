/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan9_Decorator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author hv
 */
public class Example_Regex {
    public static void main(String[] args) {
        //String regEx = "[A-Z]{1}"; // just only one time
        //String regEx = "[A-Z]{1,9}"; // just only one to 9 time
        //String regEx = "[A-Za-z0-9_!@#]{1}"; // just only one time but can be lower char, Upper char, or _ ! @ #
        //String regEx = "A(BC){2}"; // (BC have to show up 2 time
        //String regEx = "[A-Z|0-3]"; // only 1 char from A-Z or 0-3  1 time
        String regEx = "[^\\s]+\\.(?i)(png|gif|jpg|bmp)";
        String input = "abc.JPG";
        Pattern p  = Pattern.compile(regEx);
        Matcher m = p.matcher(input);
        if(m.matches()){
            System.out.println("Matched");
        }
        else{
            System.out.println("Unmatched");
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OnTap.Model;

import Lib.SQLConnectLib;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BaoHa
 */
public class NguoiDungDAO {
    SQLConnectLib dbc = DB_Connect.GetInstance();
    public NguoiDungDAO(){
        
    }
    
    public ResultSet getAllUser(){
        String query = "SELECT * FROM nguoidung";
        PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);
        ResultSet result = null;
        try {
            result = preparedStatement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(NguoiDungDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public int login(String username,String pwd){
        int result = -1;
        try {
            String query = "SELECT COUNT(*) AS Dem FROM nguoidung WHERE email=? AND password=?";
            PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, pwd);
            ResultSet result_query = preparedStatement.executeQuery();
            while(result_query.next()){
                result = result_query.getInt("Dem");
            }
            if(result>0){
                return result;
            }
        } catch (SQLException ex) {
            Logger.getLogger(NguoiDungDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}

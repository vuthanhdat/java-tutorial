/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OnTap.Model;

import Lib.SQLConnectLib;
import OnTap.Entities.SanPhamEntity;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BaoHa
 */
public class SanPhamDAO {

    private SQLConnectLib dbc = DB_Connect.GetInstance();

    public SanPhamDAO() {

    }

    public List<SanPhamEntity> getAllData() {
        List<SanPhamEntity> listSanPhamEntity = new ArrayList<>();
        String query = "SELECT * FROM sanpham;";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);
        try {
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listSanPhamEntity.add(
                        new SanPhamEntity(
                                resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getDouble(5),
                                resultSet.getDouble(6),
                                resultSet.getInt(7),
                                resultSet.getDate(8),
                                resultSet.getBoolean(9),
                                resultSet.getInt(10),
                                resultSet.getInt(11)
                        )
                );
            }
            return listSanPhamEntity;
        } catch (SQLException ex) {
            Logger.getLogger(SanPhamDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listSanPhamEntity;
    }

    public List<SanPhamEntity> searchByName(String name) {
        List<SanPhamEntity> listSanPhamEntity = new ArrayList<>();
        String query = "SELECT * FROM sanpham WHERE `tensanpham` LIKE ? ;";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);
        try {
            preparedStatement.setString(1, "%" + name + "%");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listSanPhamEntity.add(
                        new SanPhamEntity(
                                resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getDouble(5),
                                resultSet.getDouble(6),
                                resultSet.getInt(7),
                                resultSet.getDate(8),
                                resultSet.getBoolean(9),
                                resultSet.getInt(10),
                                resultSet.getInt(11)
                        )
                );
            }
            return listSanPhamEntity;
        } catch (SQLException ex) {
            Logger.getLogger(SanPhamDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listSanPhamEntity;
    }

    public SanPhamEntity getRow(int id) {
        String query = "SELECT * FROM sanpham WHERE `id` = ?;";
        ResultSet resultSet = null;
        SanPhamEntity sp = null;
        PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);
        try {
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                sp = new SanPhamEntity(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getDouble(5),
                        resultSet.getDouble(6),
                        resultSet.getInt(7),
                        resultSet.getDate(8),
                        resultSet.getBoolean(9),
                        resultSet.getInt(10),
                        resultSet.getInt(11)
                );
            }
            return sp;
        } catch (SQLException ex) {
            Logger.getLogger(SanPhamDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sp;
    }

    public int insertData(SanPhamEntity sanPham) {
        int result = -1;
        String query = "INSERT INTO `sanpham` (`id`, `tensanpham`, `mota`, `hinhanh`, `dongia`, `dongiaKM`, `soluong`, `ngaytao`, `hienthi`, `id_loai`, `id_thuonghieu`) "
                + "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);
        try {
            preparedStatement.setString(1, sanPham.tensanpham);
            preparedStatement.setString(2, sanPham.mota);
            preparedStatement.setString(3, sanPham.hinhanh);
            preparedStatement.setDouble(4, sanPham.dongia);
            preparedStatement.setDouble(5, sanPham.dongiaKM);
            preparedStatement.setInt(6, sanPham.soluong);
            preparedStatement.setDate(7, sanPham.ngaytao);
            preparedStatement.setBoolean(8, sanPham.hienthi);
            preparedStatement.setInt(9, sanPham.id_loai);
            preparedStatement.setInt(10, sanPham.id_thuonghieu);
            result = preparedStatement.executeUpdate();
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(SanPhamDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public int updateData(SanPhamEntity sanPham) {
        int result = -1;
        String query = "UPDATE `sanpham` SET "
                + "`tensanpham` = ? "
                + "`mota` = ?, "
                + "`hinhanh` = ?, `dongia` = ?, "
                + "`dongiaKM` = ?, "
                + "`soluong` = ?, "
                + "`ngaytao` = ?, "
                + "`hienthi` = ?, "
                + "`id_loai` = ?, "
                + "`id_thuonghieu` = ?"
                + "WHERE `sanpham`.`id` = ?;";
        PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);
        try {
            preparedStatement.setString(1, sanPham.tensanpham);
            preparedStatement.setString(2, sanPham.mota);
            preparedStatement.setString(3, sanPham.hinhanh);
            preparedStatement.setDouble(4, sanPham.dongia);
            preparedStatement.setDouble(5, sanPham.dongiaKM);
            preparedStatement.setInt(6, sanPham.soluong);
            preparedStatement.setDate(7, sanPham.ngaytao);
            preparedStatement.setBoolean(8, sanPham.hienthi);
            preparedStatement.setInt(9, sanPham.id_loai);
            preparedStatement.setInt(10, sanPham.id_thuonghieu);
            preparedStatement.setInt(11, sanPham.id);
            result = preparedStatement.executeUpdate();
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(SanPhamDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public int deleteRow(int id) {
        int result = -1;
        String query = "DELETE FROM sanpham WHERE `id` = ? ;";
        PreparedStatement preparedStatement = dbc.returnPrepareStatement(query);
        try {
            preparedStatement.setInt(1, id);
            result = preparedStatement.executeUpdate();
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(SanPhamDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}

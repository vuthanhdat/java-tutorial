/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OnTap.Model;

import java.sql.ResultSet;

/**
 *
 * @author BaoHa
 */
public interface AbstractDAO {
    public ResultSet getAllData();
    public int insertData();
    public int updateData();
    
    
}

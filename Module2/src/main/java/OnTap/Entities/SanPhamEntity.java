/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OnTap.Entities;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author BaoHa
 */
public class SanPhamEntity implements Serializable{
    public int id;
    public String tensanpham;
    public String mota;
    public String hinhanh;
    public double dongia;
    public double dongiaKM; 
    public int soluong;
    public Date ngaytao; 
    public boolean hienthi; 
    public int id_loai; 
    public int id_thuonghieu;

    public SanPhamEntity(){
        
    }
    public SanPhamEntity(int id, String tensanpham, String mota, String hinhanh, double dongia, double dongiaKM, int soluong, Date ngaytao, boolean hienthi, int id_loai, int id_thuonghieu) {
        this.id = id;
        this.tensanpham = tensanpham;
        this.mota = mota;
        this.hinhanh = hinhanh;
        this.dongia = dongia;
        this.dongiaKM = dongiaKM;
        this.soluong = soluong;
        this.ngaytao = ngaytao;
        this.hienthi = hienthi;
        this.id_loai = id_loai;
        this.id_thuonghieu = id_thuonghieu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTensanpham() {
        return tensanpham;
    }

    public void setTensanpham(String tensanpham) {
        this.tensanpham = tensanpham;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }

    public double getDongia() {
        return dongia;
    }

    public void setDongia(double dongia) {
        this.dongia = dongia;
    }

    public double getDongiaKM() {
        return dongiaKM;
    }

    public void setDongiaKM(double dongiaKM) {
        this.dongiaKM = dongiaKM;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public Date getNgaytao() {
        return ngaytao;
    }

    public void setNgaytao(Date ngaytao) {
        this.ngaytao = ngaytao;
    }

    public boolean isHienthi() {
        return hienthi;
    }

    public void setHienthi(boolean hienthi) {
        this.hienthi = hienthi;
    }

    public int getId_loai() {
        return id_loai;
    }

    public void setId_loai(int id_loai) {
        this.id_loai = id_loai;
    }

    public int getId_thuonghieu() {
        return id_thuonghieu;
    }

    public void setId_thuonghieu(int id_thuonghieu) {
        this.id_thuonghieu = id_thuonghieu;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OnTap.Entities;

import java.io.Serializable;

/**
 *
 * @author BaoHa
 */
public class BinhChonEntity implements Serializable{
    public int id;	
    public String ten;
    public String email;
    public int diem;
    public int id_sanpham;

    public BinhChonEntity() {
    }

    
    public BinhChonEntity(int id, String ten, String email, int diem, int id_sanpham) {
        this.id = id;
        this.ten = ten;
        this.email = email;
        this.diem = diem;
        this.id_sanpham = id_sanpham;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getDiem() {
        return diem;
    }

    public void setDiem(int diem) {
        this.diem = diem;
    }

    public int getId_sanpham() {
        return id_sanpham;
    }

    public void setId_sanpham(int id_sanpham) {
        this.id_sanpham = id_sanpham;
    }
    
}

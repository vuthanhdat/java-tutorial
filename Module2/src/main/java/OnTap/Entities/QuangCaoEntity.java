/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OnTap.Entities;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author BaoHa
 */
public class QuangCaoEntity  implements Serializable{
    public int id;
    public String hinhanh;
    public String thongdiep;
    public String thongtinchitiet;
    public Date ngaydang;

    public QuangCaoEntity(){
        
    }
    public QuangCaoEntity(int id, String hinhanh, String thongdiep, String thongtinchitiet, Date ngaydang) {
        this.id = id;
        this.hinhanh = hinhanh;
        this.thongdiep = thongdiep;
        this.thongtinchitiet = thongtinchitiet;
        this.ngaydang = ngaydang;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }

    public String getThongdiep() {
        return thongdiep;
    }

    public void setThongdiep(String thongdiep) {
        this.thongdiep = thongdiep;
    }

    public String getThongtinchitiet() {
        return thongtinchitiet;
    }

    public void setThongtinchitiet(String thongtinchitiet) {
        this.thongtinchitiet = thongtinchitiet;
    }

    public Date getNgaydang() {
        return ngaydang;
    }

    public void setNgaydang(Date ngaydang) {
        this.ngaydang = ngaydang;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OnTap.Entities;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author BaoHa
 */
public class DonHangEntity implements Serializable{
    public int id;
    public int id_khachhang;
    public Date ngaydathang;
    public String tennguoinhanhang;
    public String dienthoainguoinhan;
    public String diachigiaohang;
    public String ghichu;
    public int thanhtoan;
    public int id_trangthai;

    public DonHangEntity() {
    }

    
    public DonHangEntity(int id, int id_khachhang, Date ngaydathang, String tennguoinhanhang, String dienthoainguoinhan, String diachigiaohang, String ghichu, int thanhtoan, int id_trangthai) {
        this.id = id;
        this.id_khachhang = id_khachhang;
        this.ngaydathang = ngaydathang;
        this.tennguoinhanhang = tennguoinhanhang;
        this.dienthoainguoinhan = dienthoainguoinhan;
        this.diachigiaohang = diachigiaohang;
        this.ghichu = ghichu;
        this.thanhtoan = thanhtoan;
        this.id_trangthai = id_trangthai;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_khachhang() {
        return id_khachhang;
    }

    public void setId_khachhang(int id_khachhang) {
        this.id_khachhang = id_khachhang;
    }

    public Date getNgaydathang() {
        return ngaydathang;
    }

    public void setNgaydathang(Date ngaydathang) {
        this.ngaydathang = ngaydathang;
    }

    public String getTennguoinhanhang() {
        return tennguoinhanhang;
    }

    public void setTennguoinhanhang(String tennguoinhanhang) {
        this.tennguoinhanhang = tennguoinhanhang;
    }

    public String getDienthoainguoinhan() {
        return dienthoainguoinhan;
    }

    public void setDienthoainguoinhan(String dienthoainguoinhan) {
        this.dienthoainguoinhan = dienthoainguoinhan;
    }

    public String getDiachigiaohang() {
        return diachigiaohang;
    }

    public void setDiachigiaohang(String diachigiaohang) {
        this.diachigiaohang = diachigiaohang;
    }

    public String getGhichu() {
        return ghichu;
    }

    public void setGhichu(String ghichu) {
        this.ghichu = ghichu;
    }

    public int getThanhtoan() {
        return thanhtoan;
    }

    public void setThanhtoan(int thanhtoan) {
        this.thanhtoan = thanhtoan;
    }

    public int getId_trangthai() {
        return id_trangthai;
    }

    public void setId_trangthai(int id_trangthai) {
        this.id_trangthai = id_trangthai;
    }
    
    
    
}

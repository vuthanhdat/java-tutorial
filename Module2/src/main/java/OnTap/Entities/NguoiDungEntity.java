/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OnTap.Entities;

/**
 *
 * @author BaoHa
 */
public class NguoiDungEntity {
    public int id;
    public String email;
    public String password;
    public String hoten;
    public String diachi;
    public String dtdd;
    public int id_vaitro;
    
    public NguoiDungEntity(){
        
    }
    public NguoiDungEntity(int id, String email, String password, String hoten, String diachi, String dtdd, int id_vaitro) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.hoten = hoten;
        this.diachi = diachi;
        this.dtdd = dtdd;
        this.id_vaitro = id_vaitro;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public String getDtdd() {
        return dtdd;
    }

    public void setDtdd(String dtdd) {
        this.dtdd = dtdd;
    }

    public int getId_vaitro() {
        return id_vaitro;
    }

    public void setId_vaitro(int id_vaitro) {
        this.id_vaitro = id_vaitro;
    }
    
    
    
    
}

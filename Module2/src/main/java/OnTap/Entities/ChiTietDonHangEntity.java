/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OnTap.Entities;

import java.io.Serializable;

/**
 *
 * @author BaoHa
 */
public class ChiTietDonHangEntity implements Serializable{
    public int id;
    public int id_donhang;
    public int id_sanpham;
    public int soluong;

    public ChiTietDonHangEntity() {
    }

    
    public ChiTietDonHangEntity(int id, int id_donhang, int id_sanpham, int soluong) {
        this.id = id;
        this.id_donhang = id_donhang;
        this.id_sanpham = id_sanpham;
        this.soluong = soluong;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_donhang() {
        return id_donhang;
    }

    public void setId_donhang(int id_donhang) {
        this.id_donhang = id_donhang;
    }

    public int getId_sanpham() {
        return id_sanpham;
    }

    public void setId_sanpham(int id_sanpham) {
        this.id_sanpham = id_sanpham;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }
    
    
}

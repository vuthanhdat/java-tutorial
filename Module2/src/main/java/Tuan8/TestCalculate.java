/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan8;

/**
 *
 * @author hv
 */
public class TestCalculate {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        TamGiac tg = new TamGiac(2, 3, 5, 4, 3, 6);
        System.out.println("Square is: "+ tg.calculateSquare());
        System.out.println("Chu vi is: "+ tg.calculateC());
        
        TamGiac.Diem a = tg.new Diem(2,3);
        TamGiac.Diem b = tg.new Diem(5,4);
        TamGiac.Diem c = tg.new Diem(3,6);
        TamGiac tg1 = new TamGiac(a,b,c);
        System.out.println("Square is: "+ tg1.calculateSquare());
        System.out.println("Chu vi is: "+ tg1.calculateC());
    }
    
}

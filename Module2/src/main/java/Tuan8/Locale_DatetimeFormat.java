/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan8;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author hv
 */
public class Locale_DatetimeFormat {
    public static void main(String[] args) {
        double number = 12345.24;
        Date date = new Date();
        System.out.println(number);
        System.out.println(date);
        
        Locale lc1 = new Locale("en","US");
        NumberFormat nf1 = NumberFormat.getInstance(lc1);
        DateFormat df1 = DateFormat.getDateInstance(DateFormat.SHORT, lc1);
        System.out.println("US Standard: "+nf1.format(number));
        System.out.println("Date US Standard: "+ df1.format(date));
        
        Locale lc2 = new Locale("vi","VN");
        NumberFormat nf2 = NumberFormat.getInstance(lc2);
        DateFormat df2 = DateFormat.getDateInstance(DateFormat.SHORT, lc2);
        System.out.println("VN Standard: "+nf2.format(number));
        System.out.println("Date VN Standard: "+ df2.format(date));
        
        ResourceBundle rs = ResourceBundle.getBundle("dictionary");
        
        System.out.println(rs.getString("application"));
    }
}

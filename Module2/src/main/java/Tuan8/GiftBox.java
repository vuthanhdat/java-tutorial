/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan8;

/**
 *
 * @author BaoHa
 */
public class GiftBox {
    public String shape;
    public String color;
    final int boxPrice = 10000;

    public GiftBox() {
    }

    public GiftBox(String shape, String color) {
        this.shape = shape;
        this.color = color;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    
    public class Gift{
        public String name;
        public double weight;
        final int price = 200;

        public Gift() {
        
        }

        
        public Gift(String name, double weight) {
            this.name = name;
            this.weight = weight;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getWeight() {
            return weight;
        }

        public void setWeight(double weight) {
            this.weight = weight;
        }
        
        public double calculateFee(){
            return weight*price + GiftBox.this.boxPrice;
        }
        
        @Override
        public String toString(){
            return "Giftbox have: "+ GiftBox.this.shape + "\n"
                    + "and cover paper color: "+GiftBox.this.color+"\n"
                    + "Box price: "+ GiftBox.this.boxPrice+"\n"
                    + "Inside this giftbox have: \n"
                    + name +"\n"
                    + "this gift have weight: "+this.weight + "\n"
                    + "Total price: " + this.calculateFee();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan8;

/**
 *
 * @author hv
 */
public class TamGiac {
    public Diem diemA;
    public Diem diemB;
    public Diem diemC;
    public TamGiac() {
    }

    public TamGiac(Diem diemA, Diem diemB, Diem diemC) {
        this.diemA = diemA;
        this.diemB = diemB;
        this.diemC = diemC;
    }
    
    public TamGiac(int x1,int y1,int x2,int y2,int x3,int y3) {
        this.diemA = new Diem(x1,y1);
        this.diemB = new Diem(x2,y2);
        this.diemC = new Diem(x3,y3);
    }

    
    public Diem getDiemA() {
        return diemA;
    }

    public void setDiemA(Diem diemA) {
        this.diemA = diemA;
    }

    public Diem getDiemB() {
        return diemB;
    }

    public void setDiemB(Diem diemB) {
        this.diemB = diemB;
    }

    public Diem getDiemC() {
        return diemC;
    }

    public void setDiemC(Diem diemC) {
        this.diemC = diemC;
    }
    
    public class Diem{
        int x;
        int y;

        public Diem() {
        }

        public Diem(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }
        
        public double calulateLenght(Diem d){
            double dx = x-d.x;
            double dy = y-d.y;
            return Math.sqrt(dx*dx + dy*dy);
        }
    }
    
    public double calculateSquare(){
        double ab,bc,ac,p;
        ab = diemA.calulateLenght(diemB);
        ac = diemC.calulateLenght(diemA);
        bc = diemB.calulateLenght(diemC);
        p = (ab+ac+bc)/2;
        return Math.sqrt(p*(p-ab)*(p-ac)*(p-bc));
    }
    
    public double calculateC(){
        double ab,bc,ac,p;
        ab = diemA.calulateLenght(diemB);
        ac = diemC.calulateLenght(diemA);
        bc = diemB.calulateLenght(diemC);
        return ab+ac+bc;
    }
    
    
}

-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2017 at 01:26 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `studentmanagement`
--
CREATE DATABASE IF NOT EXISTS `studentmanagement` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `studentmanagement`;

-- --------------------------------------------------------

--
-- Table structure for table `khoa`
--

CREATE TABLE IF NOT EXISTS `khoa` (
  `MaKhoa` int(11) NOT NULL AUTO_INCREMENT,
  `TenKhoa` varchar(25) NOT NULL,
  PRIMARY KEY (`MaKhoa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `khoa`
--

INSERT INTO `khoa` (`MaKhoa`, `TenKhoa`) VALUES
(1, 'CNTT'),
(2, 'HTTT'),
(3, 'KTPM'),
(4, 'Kinh tế');

-- --------------------------------------------------------

--
-- Table structure for table `sinhvien`
--

CREATE TABLE IF NOT EXISTS `sinhvien` (
  `MaSV` int(11) NOT NULL AUTO_INCREMENT,
  `HoSV` varchar(25) NOT NULL,
  `TenSV` varchar(10) NOT NULL,
  `NgaySinh` date NOT NULL,
  `Gender` tinyint(1) NOT NULL,
  `MaKhoa` int(11) NOT NULL,
  PRIMARY KEY (`MaSV`),
  KEY `MaKhoa` (`MaKhoa`),
  KEY `MaKhoa_2` (`MaKhoa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `sinhvien`
--

INSERT INTO `sinhvien` (`MaSV`, `HoSV`, `TenSV`, `NgaySinh`, `Gender`, `MaKhoa`) VALUES
(1, 'Nguyễn Văn', 'A', '1997-04-02', 1, 1),
(2, 'Trần Văn ', 'B', '1997-04-04', 1, 2),
(3, 'Trần Như', 'Thức', '1995-08-02', 1, 3),
(4, 'Trần Bé', 'Tèo', '1996-08-02', 1, 4);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD CONSTRAINT `fk_Sinhvien_Khoa` FOREIGN KEY (`MaKhoa`) REFERENCES `khoa` (`MaKhoa`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

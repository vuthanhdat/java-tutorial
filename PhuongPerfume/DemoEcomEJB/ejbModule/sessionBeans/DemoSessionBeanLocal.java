package sessionBeans;

import javax.ejb.Local;

@Local
public interface DemoSessionBeanLocal {
	void hello();
}
